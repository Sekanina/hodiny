//#ifndef _RTC_H_
//#define _RTC_H_
#include <stdint.h>
/**
*	
*	@file DS3231.h
    
*	@brief Funkce pro ovladani modulu RTC
*/
typedef struct
{

	uint8_t  sec;
	uint8_t   min;
	uint8_t   hour;
	uint8_t  weekDay;
	uint8_t   date;
	uint8_t   month;
	uint8_t   year;
}rtc_t;

#define C_Ds3231ReadMode   0xD1  // 1101000 -1 => D1
#define C_Ds3231WriteMode  0xD0  // 1101000 -0 => D0

#define C_Ds3231SecondRegAddress   0x00   // Adresa sekundoveho registru
#define C_Ds3231TempRegAddress     0x11  // Adresa teploty registru
#define C_Ds3231ControlRegAddress  0x0E   // Adresa kontrolniho registru







uint8_t dec2bcd(uint8_t d);/**< \brief Funkce na prevod dekadicke hodnoty na BCD */
uint8_t bcd2dec(uint8_t b);/**< \brief Funkce na prevod BCD hodnoty na dekadickou */

void RTC_Init(void);/**< \brief Funkce na inicializaci RTC */
/** \brief Funkce ziskani teploty z RTC obvodu 
	\return int8_t temp
	\note  Funkce zjistuje pouze prvnich osm hornich bitu, ve spodnich osmi je pouze desetinna cast
*/
int8_t  RTC_GetTemp(void);
/** \brief Funkce na nastaveni casu RTC 
	\return none
	\note Datum a cas je v BCD formatu, proto se musi volat funkce  @ref dec2bcd
	\par - Vstup je hodnota pointru na strukturu rtc_t
	     - struktura obsahuje sekundy, minuty, hodiny, den v tydnu, rok, mesic, den
*/
void RTC_SetDateTime(rtc_t *rtc);
/** \brief Funkce na ziskani casu z RTC 
	\note Datum a cas se musi ukladat v kodu BCD, proto se musi volat funkce  @ref bcd2dec
	\return none
	\par - Vystup je hodnota pointru na strukturu rtc_t
*/
void RTC_GetDateTime(rtc_t *rtc);

//#endif
