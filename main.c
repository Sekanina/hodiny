/**
@mainpage main.c
@brief Hlavni program 
@file main.c
@brief Hlavni program 

@see DS3231.h
@brief Funkce pro ovladani modulu RTC <br>

@see i2c.h
@brief Prevzate funkce pro komunikaci I2C  <br>

@see eeprom.h
@brief Prevzate funkce pro zapis/cteni EEPROM <br>

*/
#define F_CPU	16000000UL //16MHz
#define __DELAY_BACKWARD_COMPATIBLE__// pro delay
#include <util/delay.h>
#include <avr/io.h>
#include <avr/sfr_defs.h>
#include <avr/interrupt.h>

/** \brief Definice pauzy multiplexu*/
#define pauza_seg 0.5
/** \brief Definice pinu tlacitka ENTER*/
#define pin_ent 2//PC2//0b0000100
/** \brief Definice pinu tlacitka PLUS*/
#define pin_plus 0//PC0//0b0000001
/** \brief Definice pinu tlacitka MINUS*/
#define pin_minus 1//PC1//0b0000010
/** \brief Definice portu pro tlacitka*/
#define port_pin PINC





#include "DS3231.h"
#include "i2c.h"
#include "eeprom.h"

volatile uint8_t hodiny = 0;
volatile uint8_t minuty = 0;
volatile uint8_t sekundy = 0;
volatile uint8_t zacatek = 0;
volatile uint8_t mesic, rok, den, dvt, letni, allow_temp;
int8_t teplota;
volatile uint16_t pauza;
rtc_t rtc;




/** \brief Vypis jednotlivych cislic*/
uint8_t anody[] = { //       pozice -cislo
	//0b0abcdefg
	0b01111110,		//0
	0b00110000,		//1
	0b01101101,		//2
	0b01111001,		//3
	0b00110011,		//4
	0b01011011,		//5
	0b01011111,	    //6
	0b01110000,		//7
	0b01111111,		//8
	0b01111011,		//9
};
/** \brief Vypis pismen pro den v tydnu a letni/ zimni cas*/
uint8_t text[] = { //       pozice -cislo
	0b01100111,    // P 0
	0b00111110,    //U  1
	0b01011011,    //S  2
	0b01001110,    //C  3
	0b01110110,    //N  4
	
	0b00011101,    //o 5
	0b01111101,    //a 6
	0b01101111,    //e  7
	0b00001111,    //t  8
	0b00001110,	   //L  9
	0b01101101,	   //Z(2) 10
	0b01100011,	  // � 11
	
};

// dekladrace funkc�
void init(void);/**< @brief inicializace - nastaveni preruseni, nacteni casu z RTC*/
void led_init(void);/**< @brief Vygeneruje impulz pro reset obvodu 4017*/
ISR(TIMER1_COMPA_vect);
uint8_t segmentnabit(uint8_t cislo);
void pulz(uint16_t pauza);/**< @brief Vygeneruje impulz pro clk obvodu 4017*/
void setup(void);/**< @brief Vygeneruje impulz pro clk obvodu 4017*/
void show(uint8_t cislo1);/**< @brief Zobrazeni nastavovane hodnoty na displeji  */
void showdvt(uint8_t cislo2);/**< @brief Zobrazeni dne v tydnu ve forme pismene  */
void  RTC(void);/**< @brief Nacteni casu z RTC  */
void RTC_set(void);/**< @brief Zapsani nastaveneho casu do RTC  */


void showdvt(uint8_t cislo2)
{
	uint8_t m ,n ,j;

	m=0;
	n=0;
	switch(dvt)
	{
		case 1: //po
		m=0;
		n=5;
		break;
		case 2: //ut
		m=1;
		n=8;
		break;
		case 3: //st
		m=2;
		n=8;
		break;
		case 4://ct
		m=3;
		n=8;
		break;
		case 5://p�
		m=0;
		n=6;
		break;
		case 6://so
		m=2;
		n=5;
		break;
		case 7:
		m=4;
		n=7;
		break;
	}
	// misto delay je for abych zpomalil zakladni smycku
	// delay nemuzu pouzit zustane viset posledni digit na displaji
	for(j=0;j<=200;j++)
	{
		PORTD = text[m];
		PORTB = _BV(PB2);//0b00000100;
		_delay_ms(pauza_seg);
		PORTD = text[n];
		PORTB = _BV(PB1);		//0b00000010;
		_delay_ms(pauza_seg);
	}
}

void show(uint8_t cislo1)
{
	char i;
	for(i=0;i<=100;i++)
	{
		PORTD = segmentnabit(cislo1%10); // Desitky
		PORTB = _BV(PB1);//0b00000010;
		_delay_ms(pauza_seg);
		PORTD = segmentnabit(cislo1/10); // Jednotky
		PORTB =_BV(PB2);// 0b00000100;
		_delay_ms(pauza_seg);
	}
	
}
/**
* Zde probiha nastaveni jednotlivych hodnot uzivatelem <br>
@note Funkce funguje jako stavovy automat, s kazdym potvrzenim se posune o jednu pozici dale.
*/
void setup(void) // nastaveni hodin po resetu RTC
{
	uint8_t i;
	uint8_t krok=0;
	uint8_t denmax=31;
	zacatek=2;
	
	// nastaveni sekund
	switch(krok)
	{
		case 0:  //hodiny
		_delay_ms(1000);
		while(1)
		{
			
			if(bit_is_clear(port_pin,pin_plus)) hodiny++; //PC2 -> +
			if(bit_is_clear(port_pin,pin_minus)) hodiny--;  //PC1 -> -
			if (hodiny>23) hodiny=0;
			if (hodiny<0) hodiny=23;
			show(hodiny);
			if(bit_is_clear(port_pin,pin_ent)) // PC0 -> enter
			{
				krok++;
				pulz(1);
				break; //vyzkoceni z while
			}
		}
		
		
		

		case 1:  // minuty
		_delay_ms(1000);
		//PINC=0x00;
		while(1)
		{
			if(bit_is_clear(port_pin,pin_plus)) minuty++; //PC2 -> +
			if(bit_is_clear(port_pin,pin_minus)) minuty--;  //PC1 -> -
			if (minuty>59) minuty=0;
			if (minuty<0) minuty=59;
			show(minuty);
			if(bit_is_clear(port_pin,pin_ent)) // PC0 -> enter
			{
				krok++;
				pulz(1);
				break; //vyzkoceni z while
			}
		}
		case 2:  //sekundy
		_delay_ms(1000);
		//PINC=0x00;
		while(1)
		{
			if(bit_is_clear(port_pin,pin_plus)) sekundy++;//PC2 -> +
			if(bit_is_clear(port_pin,pin_minus)) sekundy--; //PC1 -> -
			if (sekundy>59) sekundy=0;
			if (sekundy<0) sekundy=59;
			show(sekundy);
			if(bit_is_clear(port_pin,pin_ent))// PC0 -> enter
			{
				krok++;
				pulz(1);
				break; //vyzkoceni z while
			}
			
			
		}
		
		case 3:  //den v tydnu 1-po,2-ut ,3-st, 4-ct, 5-pa,6-so,7-ne
		_delay_ms(1000);
		//PINC=0x00;
		while(1)
		{
			if(bit_is_clear(port_pin,pin_plus))dvt++;//PC2 -> +
			if(bit_is_clear(port_pin,pin_minus)) dvt--; //PC1 -> -
			if (dvt>7) dvt=1;
			if (dvt<1) dvt=7;
			showdvt(dvt);
			//show(dvt);
			
			if(bit_is_clear(port_pin,pin_ent)) // PC0 -> enter
			{
				krok++;
				pulz(1);
				break; //vyzkoceni z while
			}
		}
		
		case 4:  //rok
		_delay_ms(1000);
		//	PINC=0x00;
		//rok=17; // abych nepocital od nuly
		while(1)
		{
			
			//if((PINC & pin_plus)==0) rok++; //PC2 -> +
			//if((PINC & pin_minus)==0) rok--; //PC1 -> -
			if(bit_is_clear(port_pin,pin_plus)) rok++;
			if(bit_is_clear(port_pin,pin_minus)) rok--;
			if (rok>99) rok=0;
			if (rok<0) rok=99;
			show(rok);
			if(bit_is_clear(port_pin,pin_ent))
			//if((PINC & pin_ent)==0) // PC0 -> enter
			{
				krok++;
				pulz(1);
				break; //vyzkoceni z while
			}
		}
		

		case 5:  //mesic
		//PINC=0x00;
		_delay_ms(1000);
		while(1)
		{
			if(bit_is_clear(port_pin,pin_plus)) mesic++; //PC2 -> +
			if(bit_is_clear(port_pin,pin_minus)) mesic--; //PC1 -> -
			if (mesic>12) mesic=1;
			if (mesic<1) mesic=12;
			show(mesic);
			if(bit_is_clear(port_pin,pin_ent)) // PC0 -> enter
			{
				krok++;
				pulz(1);
				break; //vyzkoceni z while
			}
		}
		case 6:  //den
		_delay_ms(1000);
		//	PINC=0x00;
		while(1)
		{
			if(bit_is_clear(port_pin,pin_plus)) den++;//PC2 -> +
			if(bit_is_clear(port_pin,pin_minus)) den--; //PC1 -> -
			
			if ((mesic==1)||(mesic==3)||(mesic==5)||(mesic==7)||(mesic==8)||(mesic==10)||(mesic==12)) denmax=31;
			if ((mesic==4)||(mesic==6)||(mesic==9)||(mesic==11)) denmax=30;
			if(mesic==2) denmax=29;
			
			if (den>denmax) den=1;
			if (den<1) den=denmax;
			show(den);
			if(bit_is_clear(port_pin,pin_ent)) // PC0 -> enter
			{
				krok++;
				pulz(1);
				break; //vyzkoceni z while
			}
		}
		case 7:// zmena letni/zimni cas
		
		_delay_ms(1000);
		//PINC=0x00;
		
		letni=EEPROM_ReadByte(0);
		_delay_ms(10);
		
		
		
		for(i=0;i<100;i++)
		{
			if (letni==1)
			{
				PORTD = text[9]; // pismeno L
				PORTB = _BV(PB2);//0b00000100;
				_delay_ms(pauza_seg);
			}
			else
			{
				PORTD = text[10]; // pismeno Z
				PORTB = _BV(PB2);//0b00000100;
				_delay_ms(pauza_seg);
			}
		}
		
		
		while(1)
		{
			//if((PINC & pin_plus)==0) //PC2 -> +
			if (bit_is_clear(port_pin,pin_plus))
			{
				letni=1;
				for(i=0;i<100;i++)
				{
					PORTD = text[9]; // pismeno L
					PORTB = _BV(PB2);
					_delay_ms(pauza_seg);
				}
			}
			//if((PINC & pin_minus)==0)  //PC1 -> -
			if (bit_is_clear(port_pin,pin_minus))
			{
				letni=0;
				for(i=0;i<100;i++)
				{
					PORTD = text[10]; // pismeno Z
					PORTB = _BV(PB2);
					_delay_ms(pauza_seg);
				}
			}
			if (bit_is_clear(port_pin,pin_ent))// PC0 -> enter
			{
				
				
				EEPROM_WriteByte(0,letni); // zapis do eepromky
				krok++;
				pulz(1);
				
				break;// vyzko�en� z while
			}
		}
		
		case 8:// povoleni teploty
		
		_delay_ms(1000);
		
		
		allow_temp=EEPROM_ReadByte(1);
		_delay_ms(10);
		
		
		
		for(i=0;i<100;i++)
		{
			if (allow_temp==1)
			{
				PORTD = text[6]; // pismeno A
				PORTB = _BV(PB2);//0b00000100;
				_delay_ms(pauza_seg);
			}
			else
			{
				PORTD = text[4]; // pismeno N
				PORTB = _BV(PB2);//0b00000100;
				_delay_ms(pauza_seg);
			}
		}
		
		
		while(1)
		{
			//if((PINC & pin_plus)==0) //PC2 -> +
			if (bit_is_clear(port_pin,pin_plus))
			{
				allow_temp=1;
				for(i=0;i<100;i++)
				{
					PORTD = text[6]; // pismeno A
					PORTB = _BV(PB2);
					_delay_ms(pauza_seg);
				}
			}
			//if((PINC & pin_minus)==0)  //PC1 -> -
			if (bit_is_clear(port_pin,pin_minus))
			{
				allow_temp=0;
				for(i=0;i<100;i++)
				{
					PORTD = text[4]; // pismeno N
					PORTB = _BV(PB2);
					_delay_ms(pauza_seg);
				}
			}
			if (bit_is_clear(port_pin,pin_ent))// PC0 -> enter
			{
				
				
				EEPROM_WriteByte(1,allow_temp); // zapis do eepromky
				break;// vyzko�en� z while
			}
		}
		
		break;
	}// konec case
	/*
	rtc.hour=dec2bcd(hodiny);// hodina
	rtc.min=dec2bcd(minuty);// minuta
	rtc.sec=dec2bcd(sekundy);// sekunda
	rtc.date=dec2bcd(den);// den
	rtc.month=dec2bcd(mesic);//mesic
	rtc.year=dec2bcd(rok);// rok
	rtc.weekDay=dec2bcd(dvt);// den v tydnu
	RTC_SetDateTime(&rtc);
	_delay_ms(200);*/
	RTC_set();
	zacatek=0;

}

void led_init() // rst high
{
	PORTD|= _BV(PD7);
	_delay_ms(1);
	PORTD&=~_BV(PD7);
	
}

void pulz(uint16_t pauza)
{
	PORTB |=_BV(PB0); //0b00000001;  // na high
	_delay_us(pauza); // pauza 
	PORTB &=~_BV(PB0);  // na low
	
}

uint8_t segmentnabit(uint8_t cislo)
{
	uint8_t bit =anody[cislo];

	return bit;
}

/**
* Zde vypis ze struktury rtc_t do promenych, se kterymi se dale pracuje <br>
<br>
@note Funkce bere z knihovny pouze pointer na adresu struktury rtc_t.
*/
void  RTC(void)
{
	RTC_GetDateTime(&rtc);
	hodiny=bcd2dec(rtc.hour);
	minuty=bcd2dec(rtc.min);
	sekundy=bcd2dec(rtc.sec);
	den=bcd2dec(rtc.date);
	mesic=bcd2dec(rtc.month);
	rok=bcd2dec(rtc.year);
	dvt=bcd2dec(rtc.weekDay);
	zacatek=0;
	
}
/**
* Zde zapis nastavenych hodnot do struktury rtc_t <br>
Pote se preda knihovne funkce na zapis do modulu. <br>
@note Knihovne se predava pouze pointer na strukturu rtc_t
*/

void RTC_set(void)
{
	rtc.hour=dec2bcd(hodiny);// hodina
	rtc.min=dec2bcd(minuty);// minuta
	rtc.sec=dec2bcd(sekundy);// sekunda
	rtc.date=dec2bcd(den);// den
	rtc.month=dec2bcd(mesic);//mesic
	rtc.year=dec2bcd(rok);// rok
	rtc.weekDay=dec2bcd(dvt);// den v tydnu
	RTC_SetDateTime(&rtc);
	_delay_ms(200);
}

/**
* Zde probiha nastaveni preruseni <br>
Vyber smeru jednotlivych portu a zapnuti pull-up rezistoru <br>
Take se zde nacte cas z RTC modulu<br>
a vyhodnocuji se zde podminky prechodu na letni nebo zimni cas, pote probehne zapis do pameti EEPROM, kde se ulozi hodnota.<br>
@note letni = 1 pak je letni cas a EEPROM obsahuje na pozici 0 hodnotu 1
@note letni = 0 pak je zimni cas a EEPROM obsahuje na pozici 0 hodnotu 0
@note allow_temp = 0 pak je povoleni zobrazeni teploty a EEPROM obsahuje na pozici 1 hodnotu 0 a teplota se nezobrazuje
*/

void init(void)
{							 //nastaveni smeru portu
	cli();					//zakazani preruseni
	DDRD= 0xFF;				// anody vystup
	DDRB= 0xFF;				//katody vystup
	DDRC= 0x00;				//port C vstup
	PORTC=0x07;//0b0000111;	// s pullup
	
	
	
	
	TCCR1B = (1<<CS12|1<<CS10|0<<CS11|1<<WGM12); //nastaveni preruseni clk/1024=15625,  ctc mode
	OCR1A = 15625-1;  // stop hodnota ( 1s)
	TIMSK = 1<<OCIE1A; //vybrani preruseni na compare
	
	RTC_Init(); // nastav� rtc
	RTC();// na�te hodnoty

	
	sei();	// povoleni preruseni
	
	letni=EEPROM_ReadByte(0);
	allow_temp=EEPROM_ReadByte(1);

	// na letni cas
	if((den>=25)&&(mesic==3)&&(dvt==7)&&(hodiny>=2)&&(letni==0))
	{
		letni=1;
		hodiny++;
		EEPROM_WriteByte(0,letni);// zapis do eepromky
		RTC_set();
	}
	// na zimni cas
	if((den>=25)&&(mesic==10)&&(dvt==7)&&(hodiny>=3)&&(letni==1))
	{
		letni=0;
		hodiny--;
		EEPROM_WriteByte(0,letni);// zapis do eepromky
		RTC_set();
	}


	
	
}

ISR(TIMER1_COMPA_vect)		// vykon�n� preruseni timer1
{
	if (zacatek!=2)// nejsem v nastaveni
	{
		if ((zacatek==0)&&(sekundy%10==9))
		{
			led_init();
			zacatek=1;
		}
		else pulz(0.5);
	}


	sekundy++;
	if(sekundy > 59)
	{
		sekundy = 0;
		minuty++;
	}
	if(minuty > 59)
	{
		minuty = 0;
		hodiny++;
	}
	if(hodiny > 23)
	hodiny = 0;
}



int main(void)
{
	
	init();
	

	while (1)
	{    // portd - anody
		//portb- katody
		//ovladani katod
		//if((((PINC & pin_minus)==0)&&((PINC & pin_plus)==0)&&((PINC & pin_ent)==0))==1) setup();
		if (((bit_is_clear(port_pin,pin_ent))&&(bit_is_clear(port_pin,pin_minus))&&(bit_is_clear(port_pin,pin_plus)))==1) setup();// jsou stiknuty vsechny 3 tlacitka - vstup do nastaveni
		
		if ((hodiny==0) &&(minuty==0)&&(sekundy==2)) RTC(); // aktualizace dat o pulnoci
		
		if (minuty%15==0) teplota=RTC_GetTemp(); // stahne data z teplomeru jednou za 15 minut
		
		if ((minuty%15==0)&&(sekundy<30)&&(allow_temp==1))// zobrazeni teploty co 15 minut na 30 sekund jen pokud je to povoleno v nastaveni
		{
			//teplota=RTC_GetTemp();
			PORTD = text[3]; //C
			PORTB = _BV(PB4);//0b00010000;
			_delay_ms(pauza_seg);
			
			PORTD = text[11]; //�
			PORTB= _BV(PB3);//0b00001000;
			_delay_ms(pauza_seg);
			
			PORTD= segmentnabit(teplota/10);
			PORTB = _BV(PB2);
			_delay_ms(pauza_seg);
			
			PORTD = segmentnabit(teplota%10);
			PORTB = _BV(PB1);
			_delay_ms(pauza_seg);
			
			
		}
		else  // jinak cas
		{
			
			PORTD= segmentnabit(sekundy/10);
			PORTB= _BV(PB5);//0b00100000;
			_delay_ms(pauza_seg);
			
			PORTD = segmentnabit(minuty%10);
			PORTB = _BV(PB4);//0b00010000;
			_delay_ms(pauza_seg);
			
			PORTD = segmentnabit(minuty/10);
			PORTB= _BV(PB3);//0b00001000;
			_delay_ms(pauza_seg);
			
			PORTD = segmentnabit(hodiny%10);
			PORTB = _BV(PB1);//0b00000010;// prehozeno kvuli desky
			_delay_ms(pauza_seg);
			
			PORTD = segmentnabit(hodiny/10);
			PORTB = _BV(PB2);// 0b00000100;
			_delay_ms(pauza_seg);
			
		}
		


		
	}
	
	
	
}

