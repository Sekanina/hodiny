#include "DS3231.h"
#include "i2c.h"


uint8_t dec2bcd(uint8_t d)
{
	return ((d/10 * 16) + (d % 10));
}

uint8_t bcd2dec(uint8_t b)
{
	return ((b/16 * 10) + (b % 16));
}


void RTC_Init(void)
{
	I2C_Init();                             // Initialize the I2c module.
	I2C_Start();                            // Start I2C communication

	I2C_Write(C_Ds3231WriteMode);        // Connect to DS1307 by sending its ID on I2c Bus
	I2C_Write(C_Ds3231ControlRegAddress);// Select the Ds1307 ControlRegister to configure Ds1307

	I2C_Write(0x00);

	I2C_Stop();   // Stop I2C communication after initializing DS1307
	
}


void RTC_SetDateTime(rtc_t *rtc)
{
	I2C_Start();                          // Start I2C communication

	I2C_Write(C_Ds3231WriteMode);      // connect to DS1307 by sending its ID on I2c Bus
	I2C_Write(C_Ds3231SecondRegAddress); // Request sec RAM address at 00H
	
	I2C_Write(rtc->sec);                    // Write sec from RAM address 00H
	I2C_Write(rtc->min);                    // Write min from RAM address 01H
	I2C_Write(rtc->hour);                    // Write hour from RAM address 02H
	I2C_Write(rtc->weekDay);                // Write weekDay on RAM address 03H
	I2C_Write(rtc->date);                    // Write date on RAM address 04H
	I2C_Write(rtc->month);                    // Write month on RAM address 05H
	I2C_Write(rtc->year);                    // Write year on RAM address 06h

	I2C_Stop();                              // Stop I2C communication after Setting the Date
}

void RTC_GetDateTime(rtc_t *rtc)
{
	I2C_Start();                            // Start I2C communication

	I2C_Write(C_Ds3231WriteMode);        // connect to DS1307 by sending its ID on I2c Bus
	I2C_Write(C_Ds3231SecondRegAddress); // Request Sec RAM address at 00H

	I2C_Stop();                                // Stop I2C communication after selecting Sec Register

	I2C_Start();                            // Start I2C communication
	I2C_Write(C_Ds3231ReadMode);            // connect to DS1307(Read mode) by sending its ID

	rtc->sec = I2C_Read(1);                // read second and return Positive ACK
	rtc->min = I2C_Read(1);                 // read minute and return Positive ACK
	rtc->hour= I2C_Read(1);               // read hour and return Negative/No ACK
	rtc->weekDay = I2C_Read(1);           // read weekDay and return Positive ACK
	rtc->date= I2C_Read(1);              // read Date and return Positive ACK
	rtc->month=I2C_Read(1);            // read Month and return Positive ACK
	rtc->year =I2C_Read(0);             // read Year and return Negative/No ACK

	I2C_Stop();                              // Stop I2C communication after reading the Date
}

int8_t  RTC_GetTemp(void)
{
	int8_t temp;
	
	I2C_Start();                          // Start I2C communication

	I2C_Write(C_Ds3231WriteMode);      // connect to DS1307 by sending its ID on I2c Bus
	I2C_Write(C_Ds3231TempRegAddress); // Request temp RAM address at 11H
	I2C_Stop();
	
	I2C_Start();                            // Start I2C communication
	I2C_Write(C_Ds3231ReadMode);
	temp=I2C_Read(0);
	I2C_Stop();
	return temp;
}
