# Hodiny s displeji a RTC

FUNKČNOST

Zobrazení hodin pomocí sedmisegmenotvých displejů (Hodiny:Minuty:Sekundy).

Možnost nastavení času (hodiny, minuty, sekundy, den v týdnu, rok, měsíc, den).

Možnost nastavení letního/zimního času a povolení zobrazení teploty.

Každých 15 minut zobrazení teploty pokud je povolena.

@Sekanina



